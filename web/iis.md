# IIS常见问题

+ 部分图标不显示或者显示为方框

    需要设置mime，在mime配置增加以下类型
```ini
.svg        image/svg+xml
.woff       application/x-font-woff
.woff2      application/x-font-woff
```
