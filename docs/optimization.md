# 性能优化

### 数据库索引

由于升级文件不能自动添加索引，如果发现数据库性能存在问题，请先对照此文档检查索引是否添加。（2.0初始版本已经添加的索引此处省略）

| 表名 |  索引字段 |  说明
| ---------  | -------- | ------
collect_comic | status | 
collect_chapter | status | 
collect_image | status | 
collect_image | collect_pid | 



