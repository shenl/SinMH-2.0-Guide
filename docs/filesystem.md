# 图片服务器分离

图片服务器默认使用本地文件系统，圣樱漫画管理程序支持使用FTP/AWS S3/Azure/Copy/Dropbox/GridFS/Rackspace/SFTP/WebDAV /ZipArchive，具体使用说明请参考[远程附件说明](https://github.com/creocoder/yii2-flysystem/blob/master/README.md)

