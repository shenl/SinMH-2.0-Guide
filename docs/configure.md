# 系统设置

## 后台系统设置

后台系统设置系指在后台界面进行的设置，

1. 社会化分享代码，推荐使用[百度分享代码](http://share.baidu.com/code)；需要自定义设置可以使用下列JS变量`pageTitle`,`pageUrl `,`pageImage`,`pageDomain`,`pageId`；ID请修改为自己的
    
    ```html
    <div class="bdsharebuttonbox">
    <a href="#" class="bds_more" data-cmd="more"></a>
    <a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a>
    <a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a>
    <a href="#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a>
    <a href="#" class="bds_tqq" data-cmd="tqq" title="分享到腾讯微博"></a>
    <a href="#" class="bds_renren" data-cmd="renren" title="分享到人人网"></a>
    </div>
    <script>
    window._bd_share_config={
        "common":{
            "bdSnsKey":{},
            "bdText":"",
            "bdMini":"2",
            "bdMiniList":false,
            "bdPic":pageImage,
            "bdStyle":"0",
            "bdSize":"32"
            },
        "share":{},
        "image":{
            "viewList":["weixin","tsina","qzone","tqq","renren"],
            "viewText":"分享到：",
            "viewSize":"16"
            },
        "selectShare":{
            "bdContainerClass":null,
            "bdSelectMiniList":["weixin","tsina","qzone","tqq","renren"]
            }
    };
    with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];
    </script>
    ```

2. 社会化评论代码，推荐使用[友言](http://www.uyan.cc/getcode)，[畅言(需要备案)](http://changyan.kuaizhan.com/)；自定义时可以使用与分享相同的变量；UID请修改为自己的
    
    ```html
    <div id="uyan_frame"></div>
    <script >
    var uyan_config = {
         'title':pageTitle,
         'pic': pageImage,
         'du': pageDomain,
         'su': pageId
    };
    </script>
    <script type="text/javascript" src="http://v2.uyan.cc/code/uyan.js?uid=93072"></script>
    ```
    
    多说评论代码：
     
     ```html
    <!-- 多说评论框 start -->
        <div class="ds-thread" id="ds-panel"></div>
    <!-- 多说评论框 end -->
    <!-- 多说公共JS代码 start (一个网页只需插入一次) -->
    <script type="text/javascript">
    var duoshuoQuery = {short_name:"sinmh"}; // 此处请修改为自己的多说二级域名
        (function() {
            var dsPanel = document.getElementById('ds-panel');
            dsPanel.setAttribute ('data-thread-key',pageId);
            dsPanel.setAttribute('data-title',pageTitle);
            dsPanel.setAttribute('data-url',pageUrl);
            var ds = document.createElement('script');
            ds.type = 'text/javascript';ds.async = true;
            ds.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//static.duoshuo.com/embed.js';
            ds.charset = 'UTF-8';
            (document.getElementsByTagName('head')[0] 
             || document.getElementsByTagName('body')[0]).appendChild(ds);
        })();
        </script>
    <!-- 多说公共JS代码 end -->
    ```
    
    畅言评论代码：
    
    ```html
    <!--PC版-->
    <div id="SOHUCS"></div>
    <script charset="utf-8" type="text/javascript" src="http://changyan.sohu.com/upload/changyan.js" ></script>
    <script type="text/javascript">
    (function() {
        var cyPanel = document.getElementById('SOHUCS');
        cyPanel.setAttribute ('sid',pageId);
        window.changyan.api.config({
        appid: 'sinmh', // 替换为你自己的appid和conf
        conf: 'sinmh-conf',
        });
    })();
    </script>
    ```

## 通用系统参数配置

通用系统配置文件位于`src/common/config/main-local.php`，在此文件用于配置
+ 系统数据库链接`db`，
+ 缓存`cache`，
+ 邮件发送`mailer`，
+ 圣樱漫画授权`sinmhSdk`，
+ 第三方登录`authClientCollection`，
+ UCenter集成`ucenter`，
+ 开/关DEBUG；

## 通用运行参数配置
通用系统配置文件位于`src/common/config/params-local.php`，此文件用于配置
+ 程序域名`domain`，
+ JS运行参数`javascript`，
+ 采集参数`collect`；

此文件配置信息可以覆盖`src/common/config/params.php`中的配置信息，若需调整更多运行参数，可以将该文件中对应参数复制到本文件重新配置；


## 前台(frontend)参数配置

+ 前台系统参数配置文件位于`src/frontend/config/main-local.php`，其中可以配置主题信息`theme`
+ 前台运行参数配置文件位于`src/frontend/config/params-local.php`，其中可用配置默认分页数量`defaultPageSize`

## 管理后台(admin)参数配置

+ 系统管理后台运行参数配置文件位于`src/admin/config/params-local.php`，其中可用配置
    + 超级管理员用户ID`super_admin`，
    + 默认分页数量`defaultPageSize`，
    + 管理后台登录验证码`token`

## 系统管理后台(backend)参数配置

+ 系统管理后台运行参数配置文件位于`src/backend/config/params-local.php`，其中可用配置系统管理员访问权限令牌`token`


