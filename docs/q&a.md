# 常见问题

+ Q: 如何修改漫画简介和阅读页的URL？

    A: 请将`src/common/config/params.php`中`comic`项*复制并添加*到`common/config/params-local.php`进行修改；具体修改模式见注释说明
    ```php
        'comic' => [
            'dirSize' => 2000,//每个目录章节数
            'uri' => [// uri格式：sinmh.com/{path}/{name}/
                'path' => 'manhua', //sinmh.com/manhua/{attribute}/
                'attribute' => 'slug',/**支持id(漫画数字ID)，slug(漫画拼音)，name(漫画中文名称可能部分漫画无法显示)
                 * id : sinmh.com/manhua/1/
                 * slug : sinmh.com/manhua/huoyingrenzhe/
                 * name : sinmh.com/manhua/火影忍者/
                 * 上述参数可以被params.local.php进行覆盖
                 */
            ],
        ],
    ```
    
+ Q: 采集漫画可以设置保存的目录吗？如何设置？
    
    A: 请将`src/common/config/params.php`中`upload`项*复制并添加*到`common/config/params-local.php`进行修改；具体修改模式见注释说明
    
    ```php
    'upload' => [
        'maxImageSize' => 1024,//最大上次文件大小，单位K
        'cover' => [
            'path' => 'images/cover/',//封面保存路径
            'crop' => true,
            'width' => 240,
            'height' => 320,
            'quality' => 60,
        ],
        'author' => [
            'path' => 'images/author/',//封面保存路径
            'crop' => true,
            'width' => 240,
            'height' => 320,
            'quality' => 60,
        ],
        'avatar' => [
            'path' => 'images/author/',//封面保存路径
            'crop' => true,
            'width' => 320,
            'height' => 320,
            'quality' => 60,
        ],
        'block' => [
            'path' => 'images/block/',//封面模块路径
        ],
        'temp' => [
            'saveOriginal' => true,
            'path' => 'images/temp/',//临时目录
        ],
        'comic' => [
            'path' => 'images/comic/',//漫画图片\
            //'dirSize' => 2000,//每个目录章节数
        ],
    ],
    ```
    
+ Q: 开启DEBUG模式

    A : 首先打开`common/config/main-local.php`
    
    大约第56行`'bootstrap' => [],`修改为`'bootstrap' => ['debug'],`
    
    大约第76行到84行 注释去掉 `/*`,`*/`;
    
    第二步，打开需要开启debug模式的应用首页文件，例如`frontend/web/index.php`
    
    第2行`defined('YII_DEBUG') or define('YII_DEBUG', false);`修改为`defined('YII_DEBUG') or define('YII_DEBUG', true);`



    