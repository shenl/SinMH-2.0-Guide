# UCenter 配置

### 配置UC后台

1. 进入UC后台/应用管理/添加应用，设置以下参数：

    应用类型：其他
    
    应用名称：您的漫画网站名称
    
    应用的主 URL：http://您的漫画网域名/ucenter（例如：http://www.sinmh.com/ucenter）
    
    应用 IP：您的漫画网IP
    
    通信密钥：自定义32~64位字符串
    
    应用接口文件名称：uc/
    
2. 点击提交，UC会给出对应的配置信息，请复制配置信息以便接下来使用。
    
### 配置漫画网UC
1. 打开`common/config/main-local.php`,找到modules下的ucenter部分，根据上一步UC给出的配置信息填写对应的项目
2. 打开`member/config/main-local.php`,新增下面代码标记的部分

```php
$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'xxxxxxxxx',
            'enableCookieValidation'=>false,
        ],
    ],
    'bootstrap'=>['ucenter'], // 新增这一行！！！
];
return $config;
```

至此，UC配置完成，在UC后台/应用列表可以看到此应用通信成功。

### 常见通信失败原因

+ 漫画网和UC服务器通讯故障，请互相ping测试通讯是否正常
+ UC数据库禁用了远程访问，请检查UC数据库是否允许漫画网访问，并检查防火墙是否阻拦访问
+ 检查漫画网和UC通讯密钥是否一致


### 注意事项

+ 请在漫画网站没有会员时启用UC，否则会造成会员名冲突
+ 通信失败

