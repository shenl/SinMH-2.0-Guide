# 批量操作SQL

执行批量操作SQL请访问系统管理后台，执行SQL

### 采集漫画列表

+ 批量刷新下载错误的漫画

```sql
UPDATE `collect_comic` SET `status`=0 WHERE `status`=20 
```
+ 批量标记下载错误的漫画状态为忽略

```sql
UPDATE `collect_comic` SET `status`=100 WHERE `status`=20 
```

+ 批量刷新下载完成数(下载完成数不影响前台显示，尽量减少此操作)

```sql
UPDATE `collect_comic` as a INNER JOIN 
(
SELECT count(id)as count, collect_pid
FROM `collect_chapter`
WHERE `status` = 1
GROUP BY collect_pid
) as b
on a.id = b.collect_pid
SET a.resolved = b.count;
```

### 采集章节列表

+ 批量标记采集中的章节为重新下载

```sql
UPDATE `chapter` SET `status`=30 WHERE `status`=10;
```
+ 批量刷新下载错误的章节

```sql
UPDATE `collect_chapter` SET `status`=0 WHERE `status`=20;
```

+ 批量刷新某一漫画的下载错误的章节

```sql
UPDATE `collect_chapter` SET `status`=0 WHERE `status`=20 AND `comic_id`=漫画ID;
```
+ 批量刷新某一漫画采集任务的下载错误的章节

```sql
UPDATE `collect_chapter` SET `status`=0 WHERE `status`=20 AND `collect_pid`=漫画采集任务ID;
```
批量刷新指定ID的采集章节
```sql
UPDATE `collect_chapter` SET `status`=0 WHERE `id` IN ( ID1,ID2,ID3);
```
+ 批量刷新下载未审核通过的章节（谨慎操作）

```sql
UPDATE `collect_chapter`
LEFT JOIN `chapter` 
ON `collect_chapter`.chapter_id = `chapter`.`id`
SET `collect_chapter`.`status` = 0
WHERE
`chapter`.`status` != 1;
```
+ 批量标记下载错误的章节状态为忽略

```sql
UPDATE `collect_chapter` SET `status`=100 WHERE `status`=20;
```

+ 批量重置下载结果为空的章节任务

```sql
UPDATE `collect_chapter`
SET `status` = 0
WHERE
	`status` = 1
AND `chapter_id` IN (
	SELECT
		`id`
	FROM
		`chapter`
	WHERE
		`status` = 1
	AND `images` = '[]'
);
```

```sql
UPDATE `chapter`
SET `status` = 10
WHERE
	`status` = 1
AND `images` = '[]';
```

### 采集图片列表

+ 批量替换图片域名，将`http://img.a.com`替换为`http://img.b.com`

```sql
UPDATE `collect_image`
SET `source_url` = REPLACE(`source_url`, 'http://img.a.com', 'http://img.b.com')
WHERE INSTR(`source_url`,'http://img.a.com') > 0;
```

+ 批量刷新下载错误的图片

```sql
UPDATE `collect_image` SET `status`=0 WHERE `status`=20;
```

+ 批量解锁

```sql
UPDATE `collect_image` SET `status`=0 WHERE `status`=10;
```
+ 批量删除错误图片

```sql
DELETE FROM `collect_image` WHERE `status` = 20;
```


### 章节信息

+ 批量删除已经被删除的漫画下属的章节

```sql
DELETE
FROM
	chapter
WHERE
comic_id NOT IN (SELECT id FROM comic)
```


### 漫画信息


+ 批量设置下载来源为规则8

```sql
UPDATE `comic` SET `source_site`=8 WHERE `id` BETWEEN 10 AND 20;
```