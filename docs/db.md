# 数据库升级


+ 升级图片字段，支持超过500张的图片
```sql
ALTER TABLE `chapter` CHANGE `images` `images` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `collect_chapter` CHANGE `source_images` `source_images` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

```

+ 升级图片源地址字段，支持超过超长图片地址
```sql
ALTER TABLE `collect_image`
MODIFY COLUMN `source_url`  varchar(2048) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' AFTER `type`;
```
