# 圣樱漫画管理系统使用指南

## 帮助目录

+ [安装指南](docs/install.md)
+ [系统设置](docs/configure.md)
+ [主题配置](docs/theme.md)
+ [漫画采集](docs/collect.md)
+ [后台任务](docs/scheduler.md)
+ [图片服务器分离](docs/filesystem.md)
+ [常见问题](docs/q&a.md)
+ [常用SQL语句](docs/sql.md)
+ [数据库优化](docs/optimization.md)
+ [UCenter集成](docs/UCenter.md)

## 系统说明
暂无


